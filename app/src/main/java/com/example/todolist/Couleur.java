package com.example.todolist;

public class Couleur {
    int id;
    String Couleur;
    String Code;

    public Couleur(int id, String couleur, String code){
        this.id = id;
        Couleur = couleur;
        Code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCouleur() {
        return Couleur;
    }

    public void setCouleur(String couleur) {
        Couleur = couleur;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }
}
