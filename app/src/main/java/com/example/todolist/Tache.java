package com.example.todolist;

public class Tache {
    int id;
    String Titre;
    String Description;
    int Degre;
    String DateLimite;
    String DateCreation;
    int DegreCreation;

    public Tache(int id, String titre, String description, int degre, String dateLimite){
        this.id = id;
        Titre = titre;
        Description = description;
        Degre = degre;
        DateLimite = dateLimite;

    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getTitre(){
        return Titre;
    }

    public void setTitre(String titre){
        Titre = titre;
    }

    public String getDescription(){
        return Description;
    }

    public void setDescription(String description){
        Description = description;
    }

    public int getDegre(){
        return Degre;
    }

    public void setDegre(int degre){
        Degre = degre;
    }

    public String getDateLimite(){
        return DateLimite;
    }

    public void setDateLimite(String dateLimite){
        DateLimite = dateLimite;
    }

}
