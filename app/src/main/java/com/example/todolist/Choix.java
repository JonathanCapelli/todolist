package com.example.todolist;

public class Choix {
    int id;
    int Degre;
    String Code;

    public Choix(int id, int degre, String code)
    {
        this.id = id;
        Degre = degre;
        Code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDegre() {
        return Degre;
    }

    public void setDegre(int degre) {
        Degre = degre;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }
}
