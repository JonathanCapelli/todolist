package com.example.todolist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.time.LocalDate;
import java.util.Calendar;

public class ModifActivity extends AppCompatActivity {

    private Tache LaTache;
    private RadioGroup rgDegre;
    private RadioButton rbDegre;
    private Button btValider;
    private Button btRetour;
    private EditText etTitre;
    private EditText etDescription;
    private CalendarView cvDateLimite;
    private String DateLimite;
    private int degreFinal = -1;
    private String DateFinal;
    private Integer idTache;
    private View decorView;

    @Override
    public void onBackPressed() {
        // do nothing.
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modif);

        decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if (visibility == 0 )
                    decorView.setSystemUiVisibility(hideSystemBars());
            }
        });

        Intent intent = getIntent();
        idTache = intent.getIntExtra("id", -1);

        DatabaseManager manager = new DatabaseManager(this);
        LaTache = manager.getTache(idTache);

        rgDegre = (RadioGroup) findViewById(R.id.rgDegre);
        btValider = findViewById(R.id.btAjouter);
        btRetour = findViewById(R.id.btRetour);
        etTitre = findViewById(R.id.etTitre);
        etDescription = findViewById(R.id.etDescription);
        cvDateLimite = findViewById(R.id.cvDateLimite);
        cvDateLimite.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                i1++;
                String jour;
                String mois;
                if (i2 < 10)
                {
                    jour = "0"+i2;
                }else{
                    jour = Integer.toString(i2);
                }
                if (i1 < 10)
                {
                    mois = "0"+i1;
                }else{
                    mois = Integer.toString(i1);
                }
                DateFinal =  i+"-"+mois+"-"+jour;

                //Toast.makeText(getApplicationContext(), DateFinal, Toast.LENGTH_SHORT).show();
            }
        });
        //DateLimite = Long.toString(cvDateLimite.); //Changer tout pour la date, faire une zone de texte avec regex pour vérifier que c bien au format d'une date

        etTitre.setText(LaTache.getTitre());
        etDescription.setText(LaTache.getDescription());

        String date = LaTache.getDateLimite();
        String parts[] = date.split("-");

        int day = Integer.parseInt(parts[2]);
        int month = Integer.parseInt(parts[1]);
        int year = Integer.parseInt(parts[0]);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        long milliTime = calendar.getTimeInMillis();

        cvDateLimite.setDate(milliTime);

        if (LaTache.getDegre() == 0)
        {
            rgDegre.check(R.id.rb0);
        }
        if (LaTache.getDegre() == 1)
        {
            rgDegre.check(R.id.rb1);
        }
        if (LaTache.getDegre() == 2)
        {
            rgDegre.check(R.id.rb2);
        }
        if (LaTache.getDegre() == 3)
        {
            rgDegre.check(R.id.rb3);
        }





        btValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), DateFinal, Toast.LENGTH_SHORT).show();
                modifTache(LaTache.id,etTitre.getText().toString(), etDescription.getText().toString(), DateFinal, degreFinal);
            }
        });

        btRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toParent();

            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    private int hideSystemBars(){
        return View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    }

    public void toParent(){
        finish();
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("id", idTache);
        startActivity(intent);
    }

    public void selectionDegre(View view){

        int radioId = rgDegre.getCheckedRadioButtonId();
        rbDegre = (RadioButton) findViewById(radioId);

        //Toast.makeText(getApplicationContext(), rbDegre.getText(), Toast.LENGTH_SHORT).show();
        if (rbDegre.getText().equals("Non urgent"))
        {
            degreFinal = 0;
        }
        if (rbDegre.getText().equals("Urgent"))
        {
            degreFinal = 1;
        }
        if (rbDegre.getText().equals("Très urgent"))
        {
            degreFinal = 2;
        }
        if (rbDegre.getText().equals("Critique"))
        {
            degreFinal = 3;
        }
    }

    public void modifTache(int id, String titre, String description, String dateLimite, int degre){
        //Toast.makeText(getApplicationContext(), Today, Toast.LENGTH_SHORT).show();
        if (titre.equals(""))
        {
            Toast.makeText(getApplicationContext(), "Veuillez entrer un titre", Toast.LENGTH_SHORT).show();
        }
        if (description.equals(""))
        {
            Toast.makeText(getApplicationContext(), "Veuillez entrer une description", Toast.LENGTH_SHORT).show();
        }
        if (degre == -1)
        {
            degre = LaTache.getDegre();
        }
        if (dateLimite == null)
        {
            dateLimite = LaTache.getDateLimite();
        }
        if ((!titre.equals("")) && (!description.equals("")) && (degre != -1) && (!dateLimite.equals("")))
        {
            titre = titre.replace("'","''");
            description = description.replace("'","''");
            LocalDate LDToday = LocalDate.now();
            String sql = "update Tache set titre = '"+titre+"', description = '"+description+"', degre = '"+degre+"', datelimite = '"+dateLimite+"' WHERE id = "+id;
            //Toast.makeText(getApplicationContext(), sql, Toast.LENGTH_SHORT).show();
            DatabaseManager manager = new DatabaseManager(this);
            manager.modifTache(sql);
            Toast.makeText(getApplicationContext(), "Tâche modifiée !", Toast.LENGTH_SHORT).show();
            toParent();
        }
    }
}