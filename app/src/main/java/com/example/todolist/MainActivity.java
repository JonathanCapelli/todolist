package com.example.todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Tache> listTache;
    private DatabaseManager dbm;
    ListView lvListe;
    private Button btAjouter;
    private Button btParametres;
    private View decorView;


    @Override
    public void onBackPressed() {
        // do nothing.
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if (visibility == 0 )
                    decorView.setSystemUiVisibility(hideSystemBars());
            }
        });

        listTache = new DatabaseManager(this).lectureTache();

        lvListe = (ListView) findViewById(R.id.listTache);

        btAjouter =(Button)findViewById(R.id.btNouveau);
        btParametres = (Button) findViewById(R.id.btParametres);


        lvListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startViewActivity(i);
            }
        });

        btAjouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nouvelleTache();
            }
        });

        btParametres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openParametres();
            }
        });



    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    private int hideSystemBars(){
        return View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    }

    public void nouvelleTache(){
        Intent intent = new Intent(this, AjoutActivity.class);
        startActivity(intent);
        this.finish();
    }

    public void openParametres(){
        Intent intent = new Intent(this, ParametresActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    protected void onResume(){
        super.onResume();
        ListeAdapter listeAdapter = new ListeAdapter(this, listTache);
        lvListe.setAdapter(listeAdapter);
    }

    private void startViewActivity(int i){
        Tache uneTache = listTache.get(i);
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("id", uneTache.getId());
        startActivity(intent);
        this.finish();


    }




}