package com.example.todolist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AjoutActivity extends AppCompatActivity {

    private RadioGroup rgDegre;
    private RadioButton rbDegre;
    private Button btAjouter;
    private Button btRetour;
    private EditText etTitre;
    private EditText etDescription;
    private CalendarView cvDateLimite;
    private String DateLimite;
    private int degreFinal = -1;
    private String DateFinal;
    private View decorView;

    @Override
    public void onBackPressed() {
        // do nothing.
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout);

        decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if (visibility == 0 )
                    decorView.setSystemUiVisibility(hideSystemBars());
            }
        });

        rgDegre = (RadioGroup) findViewById(R.id.rgDegre);
        btAjouter = findViewById(R.id.btAjouter);
        btRetour = findViewById(R.id.btRetour);
        etTitre = findViewById(R.id.etTitre);
        etDescription = findViewById(R.id.etDescription);
        cvDateLimite = findViewById(R.id.cvDateLimite);
        cvDateLimite.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                i1++;
                String jour;
                String mois;
                if (i2 < 10)
                {
                    jour = "0"+i2;
                }else{
                    jour = Integer.toString(i2);
                }
                if (i1 < 10)
                {
                    mois = "0"+i1;
                }else{
                    mois = Integer.toString(i1);
                }
                DateFinal =  i+"-"+mois+"-"+jour;

                //Toast.makeText(getApplicationContext(), DateFinal, Toast.LENGTH_SHORT).show();
            }
        });
        //DateLimite = Long.toString(cvDateLimite.); //Changer tout pour la date, faire une zone de texte avec regex pour vérifier que c bien au format d'une date


        btAjouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), DateFinal, Toast.LENGTH_SHORT).show();
                ajoutTache(etTitre.getText().toString(), etDescription.getText().toString(), DateFinal, degreFinal);
            }
        });

        btRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toMain();

            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    private int hideSystemBars(){
        return View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    }

    public void toMain(){
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void selectionDegre(View view){

        int radioId = rgDegre.getCheckedRadioButtonId();
        rbDegre = (RadioButton) findViewById(radioId);

        //Toast.makeText(getApplicationContext(), rbDegre.getText(), Toast.LENGTH_SHORT).show();
        if (rbDegre.getText().equals("Non urgent"))
        {
            degreFinal = 0;
        }
        if (rbDegre.getText().equals("Urgent"))
        {
            degreFinal = 1;
        }
        if (rbDegre.getText().equals("Très urgent"))
        {
            degreFinal = 2;
        }
        if (rbDegre.getText().equals("Critique"))
        {
            degreFinal = 3;
        }
    }

    public void ajoutTache(String titre, String description, String dateLimite, int degre){
        String Today;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        Today = dtf.format(now);
        //Toast.makeText(getApplicationContext(), Today, Toast.LENGTH_SHORT).show();
        if (titre.equals(""))
        {
            Toast.makeText(getApplicationContext(), "Veuillez entrer un titre", Toast.LENGTH_SHORT).show();
        }
        if (description.equals(""))
        {
            Toast.makeText(getApplicationContext(), "Veuillez entrer une description", Toast.LENGTH_SHORT).show();
        }
        if (degre == -1)
        {
            Toast.makeText(getApplicationContext(), "Veuillez sélectionner un degré d'urgence", Toast.LENGTH_SHORT).show();
        }
        if (dateLimite == null)
        {
            dateLimite = LocalDate.now().toString();
        }
        if ((!titre.equals("")) && (!description.equals("")) && (degre != -1) && (!dateLimite.equals("")))
        {
            titre = titre.replace("'","''");
            description = description.replace("'","''");
            LocalDate LDToday = LocalDate.now();
            String sql = "insert into Tache (titre,description,degre,datelimite) values ('"+titre+"', '"+description+"', "+degre+", '"+dateLimite+"')";
            //Toast.makeText(getApplicationContext(), sql, Toast.LENGTH_SHORT).show();
            DatabaseManager manager = new DatabaseManager(this);
            manager.ajoutTache(sql);
            Toast.makeText(getApplicationContext(), "Tâche ajoutée !", Toast.LENGTH_SHORT).show();
        }
    }

}