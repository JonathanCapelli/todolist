package com.example.todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ParametresActivity extends AppCompatActivity {

    Spinner sp0;
    Spinner sp1;
    Spinner sp2;
    Spinner sp3;
    String choixcouleur0;
    String choixcouleur1;
    String choixcouleur2;
    String choixcouleur3;
    Button btValider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametres);

        DatabaseManager manager = new DatabaseManager(this);
        List<String> lesCouleurs = manager.lectureCouleur();
        List<String> lesCodes = manager.lectureCode();

        sp0 = (Spinner) findViewById(R.id.spinner0);
        sp1 = (Spinner) findViewById(R.id.spinner1);
        sp2 = (Spinner) findViewById(R.id.spinner2);
        sp3 = (Spinner) findViewById(R.id.spinner3);
        btValider = (Button) findViewById(R.id.btAjoutParam);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, lesCouleurs);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        sp0.setAdapter(adapter);
        sp1.setAdapter(adapter);
        sp2.setAdapter(adapter);
        sp3.setAdapter(adapter);




        sp0.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                choixcouleur0 = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                choixcouleur1 = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                choixcouleur2 = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                choixcouleur3 = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (choixcouleur0.equals("Rouge"))
                {
                    manager.modifChoix(0, "#FF0000");
                }
                if (choixcouleur0.equals("Orange"))
                {
                    manager.modifChoix(0, "#FFA500");
                }
                if (choixcouleur0.equals("Jaune"))
                {
                    manager.modifChoix(0, "#FFFF00");
                }
                if (choixcouleur0.equals("Vert"))
                {
                    manager.modifChoix(0, "#00FF00");
                }
                if (choixcouleur0.equals("Bleu"))
                {
                    manager.modifChoix(0, "#1900FF");
                }
                if (choixcouleur0.equals("Violet"))
                {
                    manager.modifChoix(0, "#FF00E6");
                }
                if (choixcouleur0.equals("Bleu Ciel"))
                {
                    manager.modifChoix(0, "#00CCFF");
                }
                if (choixcouleur0.equals("Mauve"))
                {
                    manager.modifChoix(0, "#7700FF");
                }
                if (choixcouleur0.equals("Rose"))
                {
                    manager.modifChoix(0, "#FF00FB");
                }
                if (choixcouleur0.equals("Lime"))
                {
                    manager.modifChoix(0, "#95FF00");
                }

                //####

                if (choixcouleur1.equals("Rouge"))
                {
                    manager.modifChoix(1, "#FF0000");
                }
                if (choixcouleur1.equals("Orange"))
                {
                    manager.modifChoix(1, "#FFA500");
                }
                if (choixcouleur1.equals("Jaune"))
                {
                    manager.modifChoix(1, "#FFFF00");
                }
                if (choixcouleur1.equals("Vert"))
                {
                    manager.modifChoix(1, "#00FF00");
                }
                if (choixcouleur1.equals("Bleu"))
                {
                    manager.modifChoix(1, "#1900FF");
                }
                if (choixcouleur1.equals("Violet"))
                {
                    manager.modifChoix(1, "#FF00E6");
                }
                if (choixcouleur1.equals("Bleu Ciel"))
                {
                    manager.modifChoix(1, "#00CCFF");
                }
                if (choixcouleur1.equals("Mauve"))
                {
                    manager.modifChoix(1, "#7700FF");
                }
                if (choixcouleur1.equals("Rose"))
                {
                    manager.modifChoix(1, "#FF00FB");
                }
                if (choixcouleur1.equals("Lime"))
                {
                    manager.modifChoix(1, "#95FF00");
                }

                //####

                if (choixcouleur2.equals("Rouge"))
                {
                    manager.modifChoix(2, "#FF0000");
                }
                if (choixcouleur2.equals("Orange"))
                {
                    manager.modifChoix(2, "#FFA500");
                }
                if (choixcouleur2.equals("Jaune"))
                {
                    manager.modifChoix(2, "#FFFF00");
                }
                if (choixcouleur2.equals("Vert"))
                {
                    manager.modifChoix(2, "#00FF00");
                }
                if (choixcouleur2.equals("Bleu"))
                {
                    manager.modifChoix(2, "#1900FF");
                }
                if (choixcouleur2.equals("Violet"))
                {
                    manager.modifChoix(2, "#FF00E6");
                }
                if (choixcouleur2.equals("Bleu Ciel"))
                {
                    manager.modifChoix(2, "#00CCFF");
                }
                if (choixcouleur2.equals("Mauve"))
                {
                    manager.modifChoix(2, "#7700FF");
                }
                if (choixcouleur2.equals("Rose"))
                {
                    manager.modifChoix(2, "#FF00FB");
                }
                if (choixcouleur2.equals("Lime"))
                {
                    manager.modifChoix(2, "#95FF00");
                }

                //###

                if (choixcouleur3.equals("Rouge"))
                {
                    manager.modifChoix(3, "#FF0000");
                }
                if (choixcouleur3.equals("Orange"))
                {
                    manager.modifChoix(3, "#FFA500");
                }
                if (choixcouleur3.equals("Jaune"))
                {
                    manager.modifChoix(3, "#FFFF00");
                }
                if (choixcouleur3.equals("Vert"))
                {
                    manager.modifChoix(3, "#00FF00");
                }
                if (choixcouleur3.equals("Bleu"))
                {
                    manager.modifChoix(3, "#1900FF");
                }
                if (choixcouleur3.equals("Violet"))
                {
                    manager.modifChoix(3, "#FF00E6");
                }
                if (choixcouleur3.equals("Bleu Ciel"))
                {
                    manager.modifChoix(3, "#00CCFF");
                }
                if (choixcouleur3.equals("Mauve"))
                {
                    manager.modifChoix(3, "#7700FF");
                }
                if (choixcouleur3.equals("Rose"))
                {
                    manager.modifChoix(3, "#FF00FB");
                }
                if (choixcouleur3.equals("Lime"))
                {
                    manager.modifChoix(3, "#95FF00");
                }

                toMain();
            }
        });

    }

    @Override
    public void onBackPressed() {
        // do nothing.
    }

    public void toMain(){
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}