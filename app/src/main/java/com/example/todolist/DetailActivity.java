package com.example.todolist;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    private TextView tvTitre;
    private TextView tvDescription;
    private TextView tvDate;
    private TextView tvDegre;
    private Button btModifier;
    private Button btSupprimer;
    private Button btRetour;
    private Tache LaTache;
    private View decorView;

    @Override
    public void onBackPressed() {
        // do nothing.
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if (visibility == 0 )
                    decorView.setSystemUiVisibility(hideSystemBars());
            }
        });

        Intent intent = getIntent();
        Integer idTache = intent.getIntExtra("id", -1);
        //String idString = Integer.toString(idTache);
        //tvTest = (TextView) findViewById(R.id.tvTest);

        //tvTest.setText(idString);

        DatabaseManager manager = new DatabaseManager(this);
        LaTache = manager.getTache(idTache);

        tvTitre = (TextView) findViewById(R.id.tvTitre);
        tvTitre.setText(LaTache.Titre);

        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvDescription.setText(LaTache.Description);

        tvDate = (TextView) findViewById(R.id.tvDate);
        tvDate.setText("Date limite: "+LaTache.DateLimite);

        tvDegre = findViewById(R.id.tvDegre);
        if (LaTache.Degre == 0)
        {
            tvDegre.setText("Non urgent");
        }
        if (LaTache.Degre == 1)
        {
            tvDegre.setText("Urgent");
        }
        if (LaTache.Degre == 2)
        {
            tvDegre.setText("Très urgent");
        }
        if (LaTache.Degre == 3)
        {
            tvDegre.setText("Critique");
        }

        btModifier = (Button) findViewById(R.id.btModifier);
        btModifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifierTache(LaTache.id);
            }
        });

        btSupprimer = (Button) findViewById(R.id.btSupprimer);
        btSupprimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alerte = new AlertDialog.Builder(DetailActivity.this);
                alerte.setTitle("Confirmer la suppression");
                alerte.setMessage("Êtes-vous sur de vouloir supprimer cette tâche? Il n'y a aucun moyen de revenir en arrière.");
                alerte.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        supprimerTache(LaTache.id);
                    }
                });
                alerte.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Nothing
                    }
                });
                alerte.show();
            }
        });

        btRetour = findViewById(R.id.btRetour);
        btRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toMain();
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    private int hideSystemBars(){
        return View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
    }

    public void modifierTache(int id)
    {
        finish();
        Intent intent = new Intent(this, ModifActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }

    public void supprimerTache(int id)
    {
        //Toast.makeText(this, "oui", Toast.LENGTH_SHORT).show();
        String sql = null;
        DatabaseManager manager = new DatabaseManager(this);
        if (id != -1)
        {
            sql = "delete from Tache where id = "+id;
        }
        if (sql != null)
        {
            manager.supprimerTache(sql);
            finish();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    public void toMain(){
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}