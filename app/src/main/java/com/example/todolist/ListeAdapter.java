package com.example.todolist;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

public class ListeAdapter extends ArrayAdapter<Tache> {
    Context context;
    public ListeAdapter(Context context, List<Tache> listeTache){
        super(context, -1, listeTache);
        this.context = context;

    }

    public View getView(int position, View convertView, ViewGroup parent){
        View view;
        Tache uneTache;
        view = null;

        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.ligne, parent, false);
        }else{
            view = convertView;
        }


        uneTache = getItem(position);
        actualiserdegres();
        TextView tvTitre = (TextView) view.findViewById(R.id.Titre);
        TextView tvDescription = (TextView) view.findViewById(R.id.Description);
        TextView tvDateLimite = (TextView) view.findViewById(R.id.tvDateLimite);
        TextView tvJoursRestants = (TextView) view.findViewById(R.id.tvJoursRestants);
        LinearLayout uneLigne = (LinearLayout) view.findViewById(R.id.uneLigne);

        LocalDate DateLimite = LocalDate.parse(uneTache.getDateLimite());
        LocalDate Today = LocalDate.now();

        int NbJoursRestants = -1;
        if (DateLimite.getYear() == Today.getYear())
        {
            NbJoursRestants = DateLimite.getDayOfYear() - Today.getDayOfYear();
        }else{
            int DiffAnnee = DateLimite.getYear() - Today.getYear();
            NbJoursRestants = (DateLimite.getDayOfYear() - Today.getDayOfYear())+DiffAnnee*365;
        }

        tvTitre.setText(uneTache.getTitre());
        tvDescription.setText(uneTache.getDescription());
        tvDateLimite.setText("Date Limite: "+uneTache.getDateLimite());
        if (NbJoursRestants < 0)
        {
            tvJoursRestants.setText("La date limite a été dépassé ("+uneTache.getDateLimite()+")");
        }
        if (NbJoursRestants == 0)
        {
            tvJoursRestants.setText("La date limite a été fixée a aujourd'hui");
        }
        if (NbJoursRestants > 0)
        {
            if (NbJoursRestants == 1)
            {
                tvJoursRestants.setText(NbJoursRestants+" jour restant");
            }
            else
            {
                tvJoursRestants.setText(NbJoursRestants+" jours restants");
            }
        }

        DatabaseManager manager = new DatabaseManager(this.context);
        String code = manager.getChoix(uneTache.getDegre());
        uneLigne.setBackgroundColor(Color.parseColor(code));

        //if (uneTache.getDegre() == 0)
        //{
        //    uneLigne.setBackgroundColor(Color.parseColor("#00FF00"));
        //}
        //if (uneTache.getDegre() == 1)
        //{
        //    uneLigne.setBackgroundColor(Color.parseColor("#FFFF00"));
        //}
        //if (uneTache.getDegre() == 2)
        //{
        //    uneLigne.setBackgroundColor(Color.parseColor("#FFA500"));
        //}
        //if (uneTache.getDegre() == 3)
        //{
        //    uneLigne.setBackgroundColor(Color.parseColor("#FF0000"));
        //}

        return view;


    }
    public void actualiserdegres(){
        DatabaseManager manager = new DatabaseManager(this.context);
        LocalDate aujourdui = LocalDate.now();
        SQLiteDatabase base;

        String sqlE = "select id, titre, description, datelimite, degre from tache  where datelimite >= date()  order by degre DESC";
        Cursor curseur = manager.getReadableDatabase().rawQuery(sqlE, null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast()){
            String date = curseur.getString(3);
            int degres = curseur.getInt(4);
            LocalDate DateLimite = LocalDate.parse(date);
            int idT = curseur.getInt(0);
            long daysBetween = Duration.between(aujourdui.atStartOfDay(), DateLimite.atStartOfDay()).toDays();
            if((daysBetween < 3) & (degres<=2))
            {
                sqlE = "UPDATE tache SET degre = 3 WHERE id = "+ idT;
                base = manager.getWritableDatabase();
                base.execSQL(sqlE);

            }
            else{
                if((daysBetween < 5) & (degres<=1))
                {
                    sqlE = "UPDATE tache SET degre = 2 WHERE id = "+ idT;
                    base = manager.getWritableDatabase();
                    base.execSQL(sqlE);
                }
                else{
                    if((daysBetween < 8) & (degres<=0))
                    {
                        sqlE = "UPDATE tache SET degre = 1 WHERE id = "+ idT;
                        base = manager.getWritableDatabase();
                        base.execSQL(sqlE);
                    }

                }

            }




            curseur.moveToNext();
        }
        curseur.close();
    }
}
