package com.example.todolist;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DatabaseManager extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "todolist.db";
    private static final int DATABASE_VERSION = 19;
    private static SQLiteDatabase bdd;

    public DatabaseManager(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        bdd = db;
        String srtSql = "create table Tache(id Integer primary key autoincrement not null, titre String, description String, degre int, datelimite Date)";
        db.execSQL(srtSql);
        srtSql = "create table Couleur(id Integer primary key autoincrement not null, couleur String, code String)";
        db.execSQL(srtSql);
        srtSql = "create table Choix(degre Integer primary key not null, code String)";
        db.execSQL(srtSql);
        String sql1 = "insert into Couleur (couleur,code) values ('Rouge','#FF0000')";
        String sql2 = "insert into Couleur (couleur,code) values ('Orange','#FFA500')";
        String sql3 = "insert into Couleur (couleur,code) values ('Jaune','#FFFF00')";
        String sql4 = "insert into Couleur (couleur,code) values ('Vert','#00FF00')";
        String sql5 = "insert into Couleur (couleur,code) values ('Bleu','#1900FF')";
        String sql6 = "insert into Couleur (couleur,code) values ('Violet','#FF00E6')";
        String sql7 = "insert into Couleur (couleur,code) values ('Bleu Ciel','#00CCFF')";
        String sql8 = "insert into Couleur (couleur,code) values ('Mauve','#7700FF')";
        String sql9 = "insert into Couleur (couleur,code) values ('Rose','#FF00FB')";
        String sql10 = "insert into Couleur (couleur,code) values ('Lime','#95FF00')";
        db.execSQL(sql1);
        db.execSQL(sql2);
        db.execSQL(sql3);
        db.execSQL(sql4);
        db.execSQL(sql5);
        db.execSQL(sql6);
        db.execSQL(sql7);
        db.execSQL(sql8);
        db.execSQL(sql9);
        db.execSQL(sql10);
        String sql11 = "insert into Choix (degre, code) values (0,'#00FF00')";
        String sql12 = "insert into Choix (degre, code) values (1,'#FFFF00')";
        String sql13 = "insert into Choix (degre, code) values (2,'#FFA500')";
        String sql14 = "insert into Choix (degre, code) values (3,'#FF0000')";
        db.execSQL(sql11);
        db.execSQL(sql12);
        db.execSQL(sql13);
        db.execSQL(sql14);
        Log.i("DATABASE", "onCreate invoqué");


    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String strSQL = "drop table Tache";
        db.execSQL(strSQL);
        strSQL = "drop table Couleur";
        db.execSQL(strSQL);
        strSQL = "drop table Choix";
        db.execSQL(strSQL);
        this.onCreate(db);
    }

    public ArrayList<Tache> lectureTache(){
        ArrayList<Tache> taches = new ArrayList<>();
        String sqlT = "select id, titre, description, degre, datelimite from Tache order by degre desc, datelimite";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlT, null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast())
        {
            Tache tache = new Tache(curseur.getInt(0), curseur.getString(1), curseur.getString(2), curseur.getInt(3), curseur.getString(4));
            taches.add(tache);
            curseur.moveToNext();
        }
        curseur.close();
        return taches;
    }

    public ArrayList<String> lectureCouleur(){
        ArrayList<String> couleurs = new ArrayList<>();
        String sqlT = "select id, couleur, code from Couleur";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlT, null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast())
        {
            Couleur couleur = new Couleur(curseur.getInt(0), curseur.getString(1), curseur.getString(2));
            couleurs.add(couleur.getCouleur());
            curseur.moveToNext();
        }
        curseur.close();
        return couleurs;
    }

    public ArrayList<String> lectureCode(){
        ArrayList<String> couleurs = new ArrayList<>();
        String sqlT = "select id, couleur, code from Couleur";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlT, null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast())
        {
            Couleur couleur = new Couleur(curseur.getInt(0), curseur.getString(1), curseur.getString(2));
            couleurs.add(couleur.getCode());
            curseur.moveToNext();
        }
        curseur.close();
        return couleurs;
    }

    public String getChoix(int degre)
    {
        String sql = "select code from Choix WHERE degre = "+degre;
        Cursor curseur = this.getReadableDatabase().rawQuery(sql, null);
        curseur.moveToFirst();
        String code = curseur.getString(0);
        curseur.close();
        return code;
    }

    public void modifChoix(int degre, String code)
    {
        String sql = "update Choix set code = '"+code+"' WHERE degre = "+degre;
        this.getWritableDatabase().execSQL(sql);
    }

    public void ajoutTache(String sql){
        this.getWritableDatabase().execSQL(sql);
    }

    public Tache getTache(int id){
        Tache tacheFinale = null;
        String sqlT = "select id, titre, description, degre, datelimite from Tache";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlT, null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast())
        {
            Tache tache = new Tache(curseur.getInt(0), curseur.getString(1), curseur.getString(2), curseur.getInt(3), curseur.getString(4));
            if (tache.id == id)
            {
                tacheFinale = tache;
            }
            curseur.moveToNext();
        }
        curseur.close();
        return tacheFinale;
    }

    public void modifTache(String sql){
        this.getWritableDatabase().execSQL(sql);
    }

    public void supprimerTache(String sql){
        this.getWritableDatabase().execSQL(sql);
    }

    public void modifDegreTache(int id, int degre){
        String sql = "UPDATE Tache set degre = "+degre+" WHERE id = "+id;
        this.getWritableDatabase().execSQL(sql);
    }
}
